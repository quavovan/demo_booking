<?php

namespace spec\App\Service;

use App\Entity\User;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;

class UserServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserService::class);
    }

    function let(EntityManagerInterface $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_create_user(EntityManagerInterface $entityManager)
    {
        $user = new User();
        $user->setEmail('test@gmail.com')
            ->setPassword('123456');
        $this->createUser($user);
        $entityManager->persist($user)->shouldBeCalledOnce();
        $entityManager->flush()->shouldBeCalledOnce();
    }

}
