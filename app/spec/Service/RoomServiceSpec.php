<?php

namespace spec\App\Service;

use App\Entity\Room;
use App\Repository\RoomRepository;
use App\Service\RoomService;
use PhpSpec\ObjectBehavior;

class RoomServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RoomService::class);
    }

    function let(RoomRepository $roomRepository)
    {
        $this->beConstructedWith($roomRepository);
    }

    function it_check_rooms_can_reserved_false(RoomRepository $roomRepository)
    {
        $room = new Room();
        $start = (new \DateTime())->format('Y-m-d');
        $end = (new \DateTime())->format('Y-m-d');
        $roomId = [1,2];
        $roomRepository->getAllAvailableRooms($start, $end, $roomId)->shouldBeCalled()->willReturn(array_fill(0, 1, $room));
        $this->checkRoomsCanBeReserved($start, $end, $roomId)->shouldReturn(false);
    }

    function it_check_rooms_can_reserved_true(RoomRepository $roomRepository)
    {
        $room = new Room();
        $start = (new \DateTime())->format('Y-m-d');
        $end = (new \DateTime())->format('Y-m-d');
        $roomId = [1,2];
        $roomRepository->getAllAvailableRooms($start, $end, $roomId)->shouldBeCalled()->willReturn(array_fill(0, 2, $room));
        $this->checkRoomsCanBeReserved($start, $end, $roomId)->shouldReturn(true);
    }

    function it_get_rooms_by_id(RoomRepository $roomRepository)
    {
        $roomId = [1,2];
        $roomRepository->findRoomsById($roomId)->shouldBeCalledOnce();
        $this->getRoomsById($roomId);
    }

    function it_get_available_rooms(RoomRepository $roomRepository)
    {
        $start = (new \DateTime())->format('Y-m-d');
        $end = (new \DateTime())->format('Y-m-d');
        $roomRepository->getAllAvailableRooms($start, $end)->shouldBeCalledOnce();
        $this->getAvailableRooms($start, $end);
    }


}
