<?php

namespace spec\App\Service;

use App\Entity\Booking;
use App\Entity\BookingRoom;
use App\Entity\Factory\BookingFactory;
use App\Entity\Factory\BookingRoomFactory;
use App\Entity\Factory\GuestFactory;
use App\Entity\Guest;
use App\Entity\Room;
use App\Service\BookingService;
use App\Service\RoomService;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;

class BookingServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(BookingService::class);
    }

    function let(EntityManagerInterface $entityManager,
                 BookingFactory         $bookingFactory,
                 GuestFactory           $guestFactory,
                 BookingRoomFactory     $bookingRoomFactory,
                 RoomService            $roomService)
    {
        $this->beConstructedWith($entityManager, $bookingFactory, $guestFactory, $bookingRoomFactory, $roomService);
    }

    function it_create_booking(GuestFactory           $guestFactory,
                               BookingFactory         $bookingFactory,
                               RoomService            $roomService,
                               BookingRoomFactory     $bookingRoomFactory,
                               EntityManagerInterface $entityManager)
    {
        $guest = new Guest();
        $data = [
            'email' => 'email@gmail.com',
            'fullName' => 'Full Name',
            'phone' => '0123456',
            'startTime' => '2022-02-20',
            'endTime' => '2022-02-25',
            'roomId' => [1,2]
        ];
        $guestFactory->create()->shouldBeCalled()->willReturn($guest);
        $booking = new Booking();
        $bookingFactory->create()->shouldBeCalled()->willReturn($booking);

        $room = new Room();
        $rooms = array_fill(0, 2, $room);
        foreach ($rooms as $room) {
            $room->setPrice('10');
        }
        $roomService->getRoomsById($data['roomId'])->shouldBeCalled()->willReturn($rooms);

        $bookingRoom = new BookingRoom();
        $bookingRoomFactory->create()->shouldBeCalledTimes(2)->willReturn($bookingRoom);

        $entityManager->persist($booking)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $this->createBooking($data);
    }
}
