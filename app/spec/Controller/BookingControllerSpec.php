<?php

namespace spec\App\Controller;

use App\Controller\BookingController;
use App\Form\BookingFormType;
use App\Model\Booking;
use App\Service\BookingService;
use App\Service\RoomService;
use App\Utils\FormHelper;
use FOS\RestBundle\Controller\ControllerTrait;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookingControllerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(BookingController::class);
    }

    protected $payload = '{"email":"email@gmail.com","fullName":"Full Name","phone":"0123456","startTime":"2022-02-20","endTime":"2022-02-25","roomId":[1,2]}';

    function let(BookingService $bookingService, RoomService $roomService)
    {
        $this->beConstructedWith($bookingService, $roomService);
    }

    function it_create_form_invalid(Request              $request,
                                    JsonResponse         $jsonResponse,
                                    FormHelper           $formHelper,
                                    FormFactoryInterface $formFactory,
                                    FormInterface        $form)
    {
        $formFactory->create(BookingFormType::class, new Booking())->willReturn($form);
        $request->getContent()->willReturn($this->payload);
        $data = json_decode($this->payload, true);
        $form->submit($data)->shouldBeCalled();
        $form->isValid()->willReturn(false);
        $formHelper->getErrorsForm($form)->willReturn([]);
        $jsonResponse->setData([
            'errors' => []
        ])->willReturn($jsonResponse);
        $jsonResponse->setStatusCode(Response::HTTP_BAD_REQUEST)->willReturn($jsonResponse);
        $this->create($request, $jsonResponse, $formHelper, $formFactory)->shouldReturn($jsonResponse);


    }

    function it_create_form_valid(Request              $request,
                                  JsonResponse         $jsonResponse,
                                  FormHelper           $formHelper,
                                  FormFactoryInterface $formFactory,
                                  FormInterface        $form,
                                  RoomService          $roomService,
                                  BookingService       $bookingService)
    {
        $request->getContent()->willReturn($this->payload);
        $data = json_decode($this->payload, true);
        $formFactory->create(BookingFormType::class, new Booking())->willReturn($form);
        $form->submit($data)->shouldBeCalled();
        $form->isValid()->willReturn(true);
        $roomService->checkRoomsCanBeReserved($data['startTime'], $data['endTime'], $data['roomId'])->shouldBeCalled()->willReturn(true);
        $bookingService->createBooking($data)->shouldBeCalled();
        $this->create($request, $jsonResponse, $formHelper, $formFactory);
    }
}
