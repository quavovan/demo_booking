<?php

namespace spec\App\Entity;

use App\Entity\Booking;
use App\Entity\BookingRoom;
use App\Entity\Factory\GuestFactory;
use App\Entity\Guest;
use App\Repository\GuestRepository;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Validator\Constraints\Date;

class BookingSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Booking::class);
    }

    function it_gets_sets()
    {
        $guest = new Guest();
        $createdAt = new \DateTimeImmutable();
        $updatedAt = new \DateTimeImmutable();
        $startTime = new \DateTime();
        $endTime = new \DateTime();
        $total = "100";

        $guest->setEmail('test@gmail.com')
            ->setPhone('01234456')
            ->setFullName('A')
            ->setAddress('address')
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable());
        $this->setTotalPrice($total)
            ->setStatus(1)
            ->setGuest($guest)
            ->setEndTime($endTime)
            ->setStartTime($startTime)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt);


        $this->getCreatedAt()->shouldReturn($createdAt);
        $this->getUpdatedAt()->shouldReturn($updatedAt);
        $this->getStartTime()->shouldReturn($startTime);
        $this->getEndTime()->shouldReturn($endTime);
        $this->getGuest()->shouldReturn($guest);
        $this->getTotalPrice()->shouldReturn($total);
        $this->getStatus()->shouldReturn(1);
    }
}
