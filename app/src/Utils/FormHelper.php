<?php
namespace App\Utils;

use Symfony\Component\Form\FormInterface;


class FormHelper
{
    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public function getErrorsForm(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface && $childErrors = $this->getErrorsForm($childForm)) {
                $errors[$childForm->getName()] = $childErrors;
            }
        }

        return $errors;
    }
}
