<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\BookingRoom;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Room::class);
    }

    // /**
    //  * @return Room[] Returns an array of Room objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Room
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param array $id
     * @return float|int|array|string
     */
    public function findRoomsById(array $id)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.id in (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return float|int|mixed|string
     */
    public function getAllAvailableRooms(string $startTime, string $endTime, array $roomId = [])
    {
        $qb = $this->getEntityManager()->getRepository(BookingRoom::class)->createQueryBuilder('br');
        $qb->select('identity(br.room)')
            ->leftJoin('br.booking', 'b')
            ->andWhere('b.status <> (:statusCancel)')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->between('b.startTime', ':start', ':end'),
                $qb->expr()->between('b.endTime', ':start', ':end')
            ));


        $qb2 = $this->createQueryBuilder('r');
        $qb2->leftJoin('r.hotel', 'h')
            ->addSelect('h.id')
            ->andWhere('h.isActive = true');

        if (!empty($roomId)) {
            $qb2->andWhere('r.id in (:roomId)')
                ->setParameter('roomId', $roomId);
        }

        $qb2->andWhere($qb2->expr()->notIn('r.id', $qb->getDQL()))
            ->setParameter('start', $startTime)
            ->setParameter('end', $endTime)
            ->setParameter('statusCancel', Booking::STATUS['CANCEL']);

        return $qb2->getQuery()->getResult();
    }
}
