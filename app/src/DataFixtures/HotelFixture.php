<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class HotelFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i <= 10; $i ++) {
            $faker = Factory::create();
            $hotel = new Hotel();
            $hotel->setName($faker->name);
            $hotel->setIsActive(true);
            $hotel->setCreatedAt(new \DateTimeImmutable());
            $hotel->setUpdatedAt(new \DateTimeImmutable());

            for ($j =0; $j <= 10; $j ++) {
                $room = new Room();
                $room->setHotel($hotel);
                $room->setCreatedAt(new \DateTimeImmutable());
                $room->setUpdatedAt(new \DateTimeImmutable());
                $room->setName($faker->name);
                $room->setPrice($faker->randomNumber(4));
                $room->setType($faker->numberBetween(0, 3));
                $room->setIsReserved(false);
                $hotel->addRooms($room);
            }
            $manager->persist($hotel);
            $manager->flush();
        }

    }
}
