<?php

namespace App\Validator;

use App\Repository\RoomRepository;
use App\Service\RoomService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsReservableRoomValidator extends ConstraintValidator
{
    private $roomService;

    public function __construct(RoomService $roomService)
    {
        $this->roomService = $roomService;
    }

    /**
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint IsReservableRoom */

        if (null === $value || '' === $value) {
            return;
        }
        $start = $constraint->getStart();
        $end = $constraint->getEnd();
        $valid = $this->roomService->checkRoomsCanBeReserved($start, $end, $value);
        if ($valid) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->addViolation();
    }
}
