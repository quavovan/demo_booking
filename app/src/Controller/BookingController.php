<?php

namespace App\Controller;

use App\Form\BookingFormType;
use App\Model\Booking;
use App\Service\BookingService;
use App\Service\RoomService;
use App\Utils\FormHelper;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class BookingController extends AbstractFOSRestController
{
    private $bookingService;
    private $roomService;

    public function __construct(BookingService $bookingService, RoomService $roomService)
    {
        $this->bookingService = $bookingService;
        $this->roomService = $roomService;
    }

    /**
     * @Rest\Post("/api/booking/create")
     * @Rest\View(statusCode=201)
     */
    public function create(Request $request, JsonResponse $jsonResponse, FormHelper $formHelper, FormFactoryInterface $formFactory)
    {
        $form = $formFactory->create(BookingFormType::class, new Booking());
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if (!$form->isValid()) {

            return $jsonResponse->setData([
                'errors' => $formHelper->getErrorsForm($form)
            ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $validRooms = $this->roomService->checkRoomsCanBeReserved($data['startTime'], $data['endTime'], $data['roomId']);
        if (!$validRooms) {
            return $jsonResponse->setData([
                'errors' => 'These rooms can not be reserved'
            ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $this->bookingService->createBooking($data);
    }
}
