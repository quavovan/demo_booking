<?php

namespace App\Controller;

use App\Service\RoomService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class RoomController extends AbstractFOSRestController
{
    private $roomService;

    public function __construct(RoomService $roomService)
    {
        $this->roomService = $roomService;
    }

    /**
     * @Rest\Get("/api/rooms/available")
     * @Rest\View(statusCode=200)
     */
    public function getAvailableRooms(Request $request)
    {
        $startTime = $request->query->get('startTime');
        $endTime = $request->query->get('endTime');

        return $this->roomService->getAvailableRooms($startTime, $endTime);
    }
}
