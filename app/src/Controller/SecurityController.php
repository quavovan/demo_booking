<?php

namespace App\Controller;

use App\Entity\Factory\UserFactory;
use App\Form\UserRegistrationFormType;
use App\Model\UserRegistrationModel;
use App\Security\LoginFormAuthenticator;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $userService;
    private $userFactory;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService, UserFactory $userFactory)
    {
        $this->userService = $userService;
        $this->userFactory = $userFactory;
    }

    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardAuthenticatorHandler
     * @param LoginFormAuthenticator $formAuthenticator
     * @return Response
     */
    public function register(Request                      $request,
                             UserPasswordEncoderInterface $passwordEncoder,
                             GuardAuthenticatorHandler    $guardAuthenticatorHandler,
                             LoginFormAuthenticator       $formAuthenticator
    ): Response
    {
        $form = $this->createForm(UserRegistrationFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserRegistrationModel $userModel */
            $userModel = $form->getData();
            $user = $this->userFactory->create();
            $user->setEmail($userModel->email);
            $user->setPassword($passwordEncoder->encodePassword($user, $userModel->plainPassword));
            $this->userService->createUser($user);

            return $guardAuthenticatorHandler->authenticateUserAndHandleSuccess($user, $request, $formAuthenticator, 'main');
        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
