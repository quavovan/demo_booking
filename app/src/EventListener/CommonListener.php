<?php

namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class CommonListener
{
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        $em = $eventArgs->getObjectManager();

        $entity->setUpdatedAt(new \DateTimeImmutable());
        if (!$entity->getId()) {
            $entity->setCreatedAt(new \DateTimeImmutable());
        }
    }
}
