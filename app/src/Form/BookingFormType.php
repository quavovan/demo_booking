<?php

namespace App\Form;

use App\Model\Booking;
use App\Validator\IsReservableRoom;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fullName')
            ->add('email')
            ->add('phone')
            ->add('startTime')
            ->add('endTime')
            ->add('address')
            ->add('roomId', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true,
//                'constraints' => [
//                    new IsReservableRoom('2022-01-26','2022-01-29')
//                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'csrf_protection' => false,
        ]);
    }
}