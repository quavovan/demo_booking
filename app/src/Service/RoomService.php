<?php

namespace App\Service;

use App\Repository\RoomRepository;


class RoomService
{
    private $roomRepository;

    /**
     * @param RoomRepository $roomRepository
     */
    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    /**
     * @param $start
     * @param $end
     * @param array $roomId
     * @return bool
     */
    public function checkRoomsCanBeReserved($start, $end, array $roomId): bool
    {
        $rooms = $this->roomRepository->getAllAvailableRooms($start, $end, $roomId);
        if (count($roomId) != count($rooms)) {
            return false;
        }
        return true;
    }

    /**
     * @param array $roomId
     * @return array|float|int|string
     */
    public function getRoomsById(array $roomId)
    {
        return $this->roomRepository->findRoomsById($roomId);
    }

    public function getAvailableRooms($startTime, $endTime)
    {
        return $this->roomRepository->getAllAvailableRooms($startTime, $endTime);
    }
}
