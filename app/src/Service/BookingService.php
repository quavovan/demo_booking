<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\Factory\BookingFactory;
use App\Entity\Factory\BookingRoomFactory;
use App\Entity\Factory\GuestFactory;
use App\Entity\Room;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class BookingService
{
    private $entityManager;
    private $bookingFactory;
    private $guestFactory;
    private $bookingRoomFactory;
    private $roomService;

    /**
     * @param EntityManagerInterface $entityManager
     * @param BookingFactory $bookingFactory
     * @param GuestFactory $guestFactory
     * @param BookingRoomFactory $bookingRoomFactory
     * @param RoomService $roomService
     */
    public function __construct(EntityManagerInterface $entityManager,
                                BookingFactory         $bookingFactory,
                                GuestFactory           $guestFactory,
                                BookingRoomFactory     $bookingRoomFactory,
                                RoomService            $roomService)
    {
        $this->entityManager = $entityManager;
        $this->bookingFactory = $bookingFactory;
        $this->guestFactory = $guestFactory;
        $this->bookingRoomFactory = $bookingRoomFactory;
        $this->roomService = $roomService;
    }

    /**
     * @param array $data
     * @return void
     * @throws Exception
     */
    public function createBooking(array $data)
    {
        $guest = $this->guestFactory->create();
        $guest->setEmail($data['email']);
        $guest->setFullName($data['fullName']);
        $guest->setPhone($data['phone']);

        $booking = $this->bookingFactory->create();
        $booking->setStartTime(new \DateTime($data['startTime']));
        $booking->setEndTime(new \DateTime($data['endTime']));
        $booking->setGuest($guest);
        $booking->setStatus(Booking::STATUS['PROCESSING']);

        $rooms = $this->roomService->getRoomsById($data['roomId']);

        $totalPrice = 0;
        /** @var Room $room */
        foreach ($rooms as $room)
        {
            $bookingRoom = $this->bookingRoomFactory->create();
            $bookingRoom->setBooking($booking);
            $bookingRoom->setRoom($room);
            $price = $room->getPrice();
            $bookingRoom->setPrice($price);
            $booking->addBookingRooms($bookingRoom);
            $totalPrice += $price;

        }
        $booking->setTotalPrice($totalPrice);

        $this->entityManager->persist($booking);
        $this->entityManager->flush();
    }
}