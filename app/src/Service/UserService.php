<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createUser($user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
