<?php

namespace App\Model;


use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationModel
{

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @UniqueUser
     */
    public $email;

    /**
     * @@Assert\NotBlank
     * @Assert\Length(min="5")
     */
    public $plainPassword;
}
