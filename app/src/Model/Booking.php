<?php

namespace App\Model;

use App\Validator\IsReservableRoom;
use Symfony\Component\Validator\Constraints as Assert;

class Booking
{
    /**
     * @Assert\NotBlank
     */
    public $fullName;
    /**
     * @Assert\NotBlank
     */
    public $phone;
    /**
     * @Assert\NotBlank
     * @Assert\Email
     */
    public $email;

    public $address;

    /**
     * @Assert\NotBlank
     */
    public $startTime;

    /**
     * @Assert\NotBlank
     */
    public $endTime;

    public $roomId;
}
