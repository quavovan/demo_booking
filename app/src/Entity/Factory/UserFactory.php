<?php

namespace App\Entity\Factory;

use App\Entity\User;

class UserFactory implements EntityFactoryInterface
{
    public function create(): User
    {
        return new User();
    }
}
