<?php

namespace App\Entity\Factory;

use App\Entity\Guest;

class GuestFactory implements EntityFactoryInterface
{
    public function create(): Guest
    {
        return new Guest();
    }
}
