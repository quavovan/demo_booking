<?php

namespace App\Entity\Factory;

use App\Entity\Booking;

class BookingFactory implements EntityFactoryInterface
{
    public function create(): Booking
    {
        return new Booking();
    }
}
