<?php

namespace App\Entity\Factory;

use App\Entity\BookingRoom;

class BookingRoomFactory implements EntityFactoryInterface
{
    public function create(): BookingRoom
    {
        return new BookingRoom();
    }
}
