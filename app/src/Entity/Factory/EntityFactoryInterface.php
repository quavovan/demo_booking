<?php

namespace App\Entity\Factory;

interface EntityFactoryInterface
{
    public function create();
}
